<?php
require '.db/require.php';

session_start();

foreach (glob("model/*.php") as $filename)
{
    include $filename;
}
foreach (glob("model/items/*.php") as $filename)
{
    include $filename;
}
foreach (glob("controllers/*.php") as $filename)
{
    include $filename;
}


$controllers = [new HomeController(), new SapController(), new CartController(), new AccountController(), new OrderController(), new AccountInfoController()];

foreach($controllers as $c) {
    // get request uri ( /sappen/pagina )
    $uri = $_SERVER['REQUEST_URI'];

    $path = explode("?", $uri)[0];

    //split uri across /'s ( [ "", "sappen", "pagina" ] )
    $split = explode("/", $path);

    // find matching controller for uri
    if ($c->doesPathMatch($uri)) {

        // pass split url to controller
        $c->viewDestination($split);
    }
}

?>