#Flevosap
Nu met 100% meer bytes

#Startdata
Na initialisatie zijn er twee accounts (user :: pass):

`admin@flevosap.nl` :: `JGNkwsYefF6YzUttqpi8MdmrFj2iJa4F`

`user@flevosap.nl` :: `XM4WhAAuD7Yap4nLEK7W7FcotkkGYc9d`

Je kan dit aanpassen in `./.db/data/1account.sql`. Wachtwoorden worden gehasht na init.

#Quickstart
0. Download of clone het project en zet het in een webfolder.
1. Maak een database. Je mag zelf een naam kiezen maar wij raden `flevosap` aan.
2. Voeg de relevante gegevens toe aan `./.db/db.ini`
   - `[hostname]`: de hostname van de computer. Als je dit niet weet kan je deze krijgen met de PHP-functie `gethostname();`
   - `host`: het IP-adres of domein van de server. Dit kan ook localhost zijn.
   - `dbname`: de databasenaam die je in stap 1 hebt toegewezen.
   - `username`: de username van een MYSQL-gebruiker met volledige rechten over de database.
   - `password`: het wachtwoord van deze MYSQL-gebruiker.
3. Voer `./init.php` uit in console. Deze is niet uitvoerbaar vanuit een webserver.
4. ???
5. Profit

#Structuur
De folder .db bevat alle informatie voor het verbinden, instellen en uitlezen van de database.
De subfolders data, relations en structure staan voor de data, foreign keys en tables respectievelijk.
`require.php` is het bestand dat je kan requiren in php (of includen, of -once) om een snel begin te maken
met databasedevelopment.
Controllers bevat controllers, docs bevat gerelateerde documenten, fragments bevat HTML-segmenten die meerdere malen worden gebruikt in views. Deze staan overegens in views.
Model bevat helperclasses voor databaseinteractie met objecten.