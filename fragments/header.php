<nav class="white">
    <div class="nav-wrapper">
        <a href="#" data-target="mobile-menu" class="sidenav-trigger">
            <i class="material-icons" style="color: black;">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>
                <a href="/">
                    <img style="height: 64px;"
                         src="https://www.flevosap.nl/wp-content/uploads/2016/07/logo-headerV3.png" alt="logo">
                </a>
            </li>
            <li><a class="black-text" href="/">Thuis</a></li>
            <li><a class="black-text" href="/sappen">Sappen</a></li>
            <li><a class="black-text" href="https://www.flevosap.nl/nieuws/" target="_blank">Nieuws</a></li>
            <?php if (Account::isLoggedIn()) { ?>
                <li><a class="black-text" href="/account">Account</a></li>
            <?php } else { ?>
                <li><a class="black-text" href="/account/login">Login</a></li>
            <?php } ?>
            <li><a class="black-text" href="/cart">Winkelwagen</a></li>
        </ul>
    </div>

    <ul class="sidenav" id="mobile-menu">
        <li><a class="black-text" href="/">Thuis</a></li>
        <li><a class="black-text" href="/sappen">Sappen</a></li>
        <li><a class="black-text" href="https://www.flevosap.nl/nieuws/" target="_blank">Nieuws</a></li>
        <?php if (isset($_SESSION['userID'])) { ?>
            <li><a class="black-text" href="/account">Account</a></li>
        <?php } else { ?>
            <li><a class="black-text" href="/account/login">Login</a></li>
        <?php } ?>
        <li><a class="black-text" href="/cart">Winkelwagen</a></li>
    </ul>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.sidenav');
            var instances = M.Sidenav.init(elems, null);
        });
    </script>
</nav>