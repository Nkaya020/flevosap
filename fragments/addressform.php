<form action=<?= $action ?> method="POST" class="registerform">
    <label for="name">Voornaam:</label>
    <input type="text" name="name" required><br><br>

    <label for="lastName">Achternaam:</label>
    <input type="text" name="lastName" required><br><br>

    <label for="city">Woonplaats:</label>
    <input type="text" name="city" required><br><br>

    <label for="address">Adres:</label>
    <input type="text" name="address" required><br><br>

    <label for="phoneNumber">Telefoonnummer:</label>
    <input type="text" name="phoneNumber" required><br><br>

    <input class="btn" type="submit" value="Adres toevoegen">
</form>