<footer class="page-footer">
    <div class="container">
        <div class="row right">
            <div class="col 16 s12">
                <h5 class="white-text">Flevosap BV</h5>
                Prof. Zuurlaan 22 <br/>
                8256 PE Biddinghuizen, Nederland<br/>
                Tel: +31 (0)321 – 33 25 25<br/>
                info@flevosap.nl
            </div>
        </div>
        <div class="col 14 offset-12 s12">
            <a href="/">
                <img src="https://www.flevosap.nl/wp-content/uploads/2016/07/logo-headerV3.png" alt="logo">
            </a>
        </div>
    </div>
</footer>
</body>
</html>