<?php

abstract class Controller
{
    /**
     * @param string $path
     * @return bool
     */
    abstract function doesPathMatch(string $path);

    /**
     * @param string[] $split
     */
    abstract function viewDestination(array $split);

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function str_contains(string $haystack, string $needle) {
        return strpos($haystack, $needle) !== false;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function str_starts_with(string $haystack, string $needle) {
        $length = strlen( $needle );
        return substr( $haystack, 0, $length ) === $needle;
    }

    /**
     * @param string $haystack
     * @param string $needle
     * @return bool
     */
    function str_ends_with(string $haystack, string $needle)
    {
        $length = strlen($needle);
        if (!$length) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }
}
?>