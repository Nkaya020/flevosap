<?php

/**
 * Class Order
 */
class Order extends Item {

    /** @var int */
    public $id;

    /** @var int */
    public $userID;

    /** @var double */
    public $price;

    /** @var string */
    public $createdAt;

    /** @var string */
    public $updatedAt;


    /**
     * @param $result
     * @return Order
     */

    static function fromDatabaseSingle($result)
    {
        $item = new Order();
        $item->id = $result['id'];
        $item->userID = $result['userID'];
        $item->price = $result['price'];
        $item->createdAt = $result['createdAt'];
        $item->updatedAt = $result['updatedAt'];
        return $item;
    }

    static function getOrdersFromUserID($userID){
        $result = query(db(), 'SELECT * FROM `orders` WHERE userID = ?', [$userID]);
        $orders = Item::fromDatabaseArray(new Order(), $result);
        return $orders;
    }

    static function getDetailedOrderInfo($orderID) {
        $db = db();
        $statement = $db->prepare("SELECT o.id, o.createdAt, o.userID,
                                            i.sapID, i.quantity, i.orderID,
                                            s.name, s.image, s.price,
                                            a.id
                                            FROM `orders` o
                                            INNER JOIN orderitem i on o.id = i.orderID
                                            INNER JOIN sappen s on i.sapID = s.id
                                            INNER JOIN account a on o.userID = a.id 
                                            WHERE i.orderID = ?
                                            GROUP BY s.id");
        $statement->execute([$orderID]);
        return $statement->fetchAll();
    }

    static function getOrdersFromOrderID($orderID){
        $result = querySingle(db(), 'SELECT * FROM `orders` WHERE `id` = ?', [$orderID]);
        $orders = Order::fromDatabaseSingle($result);
        return $orders;
    }

    static function setOrder(int $uid, int $addressID, $price, $isPayed, $items)
    {
        $orderID = insertOne(db(),
            'INSERT INTO `orders` (userID, addressID, price, isPayed) VALUE (?, ?, ?, ?);',
            [$uid, $addressID, $price, $isPayed]);

        foreach ($items as $item){
            Orderitem::setOrderitemsFromOrderID($item['sap']->id, $item['quantity'], $orderID);
        }

        return $orderID;
    }

//    static public function generateRecentOrders()
//    {
//        $result= [];
//        foreach ()
//        {
//            array_push($result, querySingle(db(), 'SELECT * FROM `orders` WHERE id = ?', [$i]));
//        }
//
//        $orders = Item::fromDatabaseArray(new Order(), $result);
//        return $orders;
//    }
}