<?php

/**
 * Class Sap
 */
class Sap extends Item {

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var double */
    public $price;

    /** @var string */
    public $nutrition;

    /** @var string */
    public $image;

    /** @var string */
    public $updatedAt;


    /**
     * @param $result
     * @return Sap
     */
    static function fromDatabaseSingle($result)
    {
        $item = new Sap();
        $item->id = $result['id'];
        $item->name = $result['name'];
        $item->description = $result['description'];
        $item->price = $result['price'];
        $item->nutrition = $result['nutrition'];
        $item->image = $result['image'];
        $item->updatedAt = $result['updatedAt'];
        return $item;
    }

    static function getAll(){
        $result = query(db(), "select * from sappen", null);
        return Item::fromDatabaseArray(new Sap(), $result);
    }

    static function getByID($id){
        $result = querySingle(db(), "select * from sappen WHERE id = ?", [$id]);
        return Sap::fromDatabaseSingle($result);
    }
}