<?php

// de klasse voor accounts
class Accountgegevens extends Item
{
    //membervariables
    public int $id;
    public int $userID;
    public string $name;
    public string $lastname;
    public string $city;
    public string $address;
    public string $phonenumber;

    static function getFromUser($id)
    {
        $result = query(db(), 'SELECT * FROM `accountgegevens` WHERE userId = ?', [$id]);
        $gegevens = Item::fromDatabaseArray(new Accountgegevens(), $result);
        return $gegevens;
    }

    static function getFromID($id)
    {
        $result = query(db(), 'SELECT * FROM `accountgegevens` WHERE id = ?', [$id]);
        $gegevens = Item::fromDatabaseArray(new Accountgegevens(), $result);
        return $gegevens;
    }

    static function createNew(int $userID, string $name, string $lastName, string $city, string $address, string $phoneNumber)
    {
        return insertOne(db(),
            'INSERT INTO `accountgegevens`(userID, name, lastname, city, address, phonenumber) 
            VALUE   (?,?,?,?,?,?);',
            [$userID, $name, $lastName, $city, $address, $phoneNumber]);
    }

    static function delete(int $id)
    {
        return insertOne(db(),
            'DELETE FROM `accountgegevens` WHERE id = ?;',
            [$id]);
    }

    static function fromDatabaseSingle($result)
    {
        //Om de de methode fromDatabaseArray aan te spreken, moet ik account van het type item maken
        $item = new Accountgegevens();
        $item->id = $result['id'];
        $item->userID = $result['userID'];
        $item->name = $result['name'];
        $item->lastname = $result['lastname'];
        $item->address = $result['address'];
        $item->city = $result['city'];
        $item->phonenumber = $result['phonenumber'];
        return $item;
    }
}