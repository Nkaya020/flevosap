<?php

class Orderitem extends Item
{

    /** @var int */
    public $id;

    /** @var int */
    public $orderID;

    /** @var int */
    public $sapID;

    /** @var int */
    public $quantity;

    /** @var string */
    public $createdAt;

    /** @var string */
    public $updatedAt;

    public $order = [];

    /**
     * @param $result(
     * @return Orderitem
     */
    static function fromDatabaseSingle($result)
    {
        $item = new Orderitem();
        $item->id = $result['id'];
        $item->orderID = $result['orderID'];
        $item->sapID = $result['sapID'];
        $item->quantity = $result['quantity'];
        $item->createdAt = $result['createdAt'];
        $item->updatedAt = $result['updatedAt'];
        return $item;
    }

    static public function getOrderitemsFromOrderID($key)
    {
        $result= query(db(),'SELECT * FROM `orderitem` WHERE orderID = ?', [$key]);
        return Item::fromDatabaseArray(new Orderitem(), $result);
    }

    static public function setOrderitemsFromOrderID(int $sapID ,int $quantity, int $orderID)
    {
        return insertOne(db(),
            'INSERT INTO `orderitem` (orderID, sapID, quantity) VALUE (?, ?, ?);',
            [$orderID, $sapID, $quantity]);
    }

    static public function getPriceFromOrderID($key)
    {
        $result= query(db(), 'SELECT price FROM `sappen`
                                    INNER JOIN `orderitem` o on sappen.id = o.sapID 
                                    WHERE o.orderID = ?', [$key]);
        return $result;
        //return Item::fromDatabaseArray()
    }

    static function createNewOrderitem(int $sapID, int $quantity)
    {
        return insertOne(db(),
            'INSERT INTO `orderitem` (sapID, quantity) VALUE (?, ?);',
            [$sapID, $quantity]);
    }

    static function finishOrder()
    {
        //vergelijk de aanmaakdatum met die van van de order
        
    }
}