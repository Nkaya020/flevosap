<?php

/**
 * Class Review
 */
class Review extends Item {

    /** @var int */
    public $id;

    /** @var int */
    public $userId;

    /** @var string */
    public $titel;

    /** @var string */
    public $description;

    /** @var string */
    public $createdAt;

    /** @var string */
    public $updatedAt;


    /**
     * @param $result
     * @return Review
     */
    static function fromDatabaseSingle($result)
    {
        $item = new Review();
        $item->id = $result['id'];
        $item->userId = $result['userId'];
        $item->titel = $result['titel'];
        $item->description = $result['description'];
        $item->createdAt = $result['createdAt'];
        $item->updatedAt = $result['updatedAt'];
        return $item;
    }
}