<?php

// de klasse voor accounts
class Account extends Item
{
    //membervariables
    public int $id;
    public string $email;
    public string $password;
    public string $createdAt;
    public string $updatedAt;

    static function fromDatabaseSingle($result)
    {
        //Om de de methode fromDatabaseArray aan te spreken, moet ik account van het type item maken
        $item = new Account();
        $item->id = $result['id'];
        $item->email = $result['email'];
        $item->password = $result['password'];
        $item->createdAt = $result['createdAt'];
        $item->updatedAt = $result['updatedAt'];

        return $item;
    }

    static function createNew(string $email, string $password){
        $password = password_hash($password, PASSWORD_DEFAULT);
        return insertOne(db(),
            'INSERT INTO `account` (email, password) VALUE (?, ?);',
            [$email, $password]);

    }

    static function isLoggedIn(){
        if(isset($_SESSION['loggedIn'])){
            return $_SESSION['loggedIn'];
        }
        return false;
    }

    static function getFromEmail($email){
        $result = querySingle(db(), 'SELECT * FROM `account` WHERE email = ?', [$email]);
        return Account::fromDatabaseSingle($result);
    }

    static function getUserFromID($id){
        $result = querySingle(db(), 'SELECT * FROM `account` WHERE id = ?', [$id]);
        return Account::fromDatabaseSingle($result);
    }

    static function tryLogIn($email, $password){
        try {
            $account = Account::getFromEmail($email);
        } catch (Exception $e) {
            return false;
        }
        if(password_verify ($password, $account->password)){
            $_SESSION['loggedIn'] = true;
            $_SESSION['userID'] = $account->id;
            return true;
        }
        return false;
    }

    public function getOders($id)       //een account doet orders,
    {                                   //de klasse order in principe niet
        Order::getOrdersFromUserID($id);
    }

    static function logout(){
        if(array_key_exists('loggedIn', $_SESSION)){
            $_SESSION['loggedIn'] = false;
            unset($_SESSION['userID']);
        }
    }
}