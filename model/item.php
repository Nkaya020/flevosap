<?php
abstract class Item {
    /**
     * @param $result
     * Databaseresultaat voor een "Item"
     * @return $this
     * Object opgebouwd vanuit het databaseobject.
     */
    static abstract function fromDatabaseSingle($result);

    /**
     * @param $result
     * Stack van databaseresultaten voor een "Item"
     * @param Item $class
     * Class voor het object wat je wilt maken. Klinkt
     * onnodig maar moet van php.
     * @return $this[]
     * Stack van objecten opgebouwd uit databaseobjecten.
     * 
     * voorbeeld:
     * Sap::fromDatabaseArray(
     */
    static function fromDatabaseArray(Item $class, $result) {
        if(sizeof($result) == 0) {
            return ;
        }

        $s = array(sizeof($result));
        $index = 0;
        
        foreach($result as $i) {
            $s[$index] = $class::fromDatabaseSingle($i);
            $index++;
        }
        return $s;
    }
}