<?php
class CartController extends Controller {

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return $this->str_starts_with($path, "/cart");
    }

    public function getTotalPrice(){
        $totalPrice = 0;
        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $productID => $quantity) {
                $sap = Sap::getByID($productID);
                $totalPrice += $sap->price * $quantity;
            }
        }
        return $totalPrice;
    }

    public function getCartItems(){
        $cartItems = [];
        if(isset($_SESSION['cart'])){
            foreach ($_SESSION['cart'] as $productID => $quantity) {
                $sap = Sap::getByID($productID);
                array_push($cartItems, ['sap' => $sap, 'quantity' => $quantity]);
            }
        }
        return $cartItems;
    }

    public function emptyCart(){
        unset($_SESSION['cart']);
    }

    /**
     * @param string[] $split
     */
    function viewDestination(array $split)
    {

        if (!isset($split[2])) {
            // IF the user requests the main page and not a subpage
            $TITLE="Flevosap: Winkelwagen";
            $cartItems = $this->getCartItems();
            $totalPrice = $this->getTotalPrice();


            include 'fragments/prefix.php';
            include 'fragments/header.php';
            include 'views/cart/index.php';
            include 'fragments/suffix.php';
        } else {
            // split[2] is het gedeelte achter de 2e slash
            switch ($split[2]) {
                case "payed":
                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    include 'views/cart/payed.php';
                    include 'fragments/suffix.php';
                    break;
                case "add":
                    if(count($_POST) > 0){
                        $_SESSION['cart'][$_POST['productID']] = $_POST['quantity'];
                    }
                    header('location: /sappen');
                    break;
                case "update":
                    if ($_POST['amount'] < 1) {
                        unset($_SESSION['cart'][$_POST['item']]);
                    } else {
                        $_SESSION['cart'][$_POST['item']] = $_POST['amount'];
                    }
                    header("location:/cart");
                    die();
                case "checkout":
                    if(count($_POST) > 0){
                        if(!isset($_POST['name'])){
                            Order::setOrder($_SESSION['userID'], $_POST['address'], $this->getTotalPrice(), true, $this->getCartItems());
                        }else{
                            if(Account::isLoggedIn()){
                                $newAddress = Accountgegevens::createNew($_SESSION['userID'], $_POST['name'], $_POST['lastName'], $_POST['city'], $_POST['address'], $_POST['phoneNumber']);
                                Order::setOrder($_SESSION['userID'], $newAddress, $this->getTotalPrice(), true, $this->getCartItems());
                            }
                        }
                        $this->emptyCart();
                        header('location:/cart/payed');
                        die();
                    }

                    $addresses = null;
                    if(isset($_SESSION['userID']))
                        $addresses = Accountgegevens::getFromUser($_SESSION['userID']);

                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    include 'views/cart/checkout.php';
                    include 'fragments/suffix.php';
                    break;
            }
        }
    }
}