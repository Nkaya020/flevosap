<?php

class HomeController extends Controller {

    /**
     * @param string[] $split
     */
    function viewDestination(array $split)
    {
        $TITLE="Flevosap";
        include 'fragments/prefix.php';
        include 'fragments/header.php';
        include 'views/home/index.php';
        include 'fragments/suffix.php';
    }

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return ($path == "" || $path == "/");
    }
}