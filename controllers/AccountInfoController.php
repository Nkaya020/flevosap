<?php

class AccountInfoController extends Controller
{

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return $this->str_starts_with($path, "/accinfo");
    }

    /**
     * @param string[] $split
     */
    function viewDestination(array $split)
    {
        if (!Account::isLoggedIn()) {
            header("location: /");
        }
        if (!isset($split[2])) {
            // IF the user requests the main page and not a subpage
            $TITLE = "Flevosap Login";
            include 'fragments/header.php';
            include 'fragments/prefix.php';
            $addresses = Accountgegevens::getFromUser($_SESSION['userID']);
            include 'views/gebruikersprofiel/index.php';
            include 'fragments/suffix.php';
        } else {
            // split[2] is het gedeelte achter de 2e slash
            switch ($split[2]) {
                case "new":
                    if (count($_POST) > 0) {
                        $userInfo = Accountgegevens::createNew($_SESSION['userID'],
                            $_POST['name'],
                            $_POST['lastName'],
                            $_POST['city'],
                            $_POST['address'],
                            $_POST['phoneNumber']
                        );
                        if (isset($_POST['payment'])) {
                            header('');
                        }else{
                            header('location: /accinfo');
                        }
                    }
                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    include 'views/gebruikersprofiel/new.php';
                    include 'fragments/suffix.php';
                    break;
                case "delete":
                    if (count($_POST) > 0) {
                        Accountgegevens::delete($_POST['id']);
                    }
                    header('location: /accinfo');
                    break;
            }
        }
    }
}