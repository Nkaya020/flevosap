<?php

class AccountController extends Controller
{

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return $this->str_starts_with($path, "/account");
    }

    /**
     * @param string[] $split
     */
    function viewDestination(array $split)
    {

        if (!isset($split[2])) {
            // IF the user requests the main page and not a subpage
            $TITLE = "Flevosap: Mijn account";
            include 'fragments/prefix.php';
            include 'fragments/header.php';
            include 'views/home/accountlanding.php';
            include 'fragments/suffix.php';
        } else {
            // split[2] is het gedeelte achter de 2e slash
            switch ($split[2]) {
                case "login":
                    if (!Account::isLoggedIn()) {
                        if (!empty($_POST) > 0) {
                            // if user is attempting to log in
                            $loggedIn = Account::tryLogIn($_POST['email'], $_POST['password']);
                            if ($loggedIn) {
                                header('location: /account');
                                exit;
                            } else {
                                header('location:/account/login?fail');
                            }
                        }
                        include 'fragments/prefix.php';
                        include 'fragments/header.php';
                        include 'views/loginPage/index.php';
                        include 'fragments/suffix.php';
                    } else {
                        // if user is already logged in
                        header('location: /account');
                    }
                    break;

                case "profielgegevens":
                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    include 'views/gebruikersprofiel/index.php';
                    include 'fragments/suffix.php';
                    break;

                case "new":
                    if(Account::isLoggedIn()) header('location: /');
                    if (!empty($_POST)) {
                        if ($_POST['password'] === $_POST['password2']) {
                            $newUserID = Account::createNew($_POST['email'], $_POST['password']);
                            Account::tryLogIn($_POST['email'], $_POST['password']);
                            header('location: /account');
                        } else {
                            header('location: /account/new');
                        }
                    } else {
                        include 'fragments/prefix.php';
                        include 'fragments/header.php';
                        include 'views/loginPage/register.php';
                        include 'fragments/suffix.php';
                    }

                    exit();
                    break;

                case "logout":
                    Account::logout();
                    header('location: /');
                    break;
            }
        }
    }
}