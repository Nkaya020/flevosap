<?php

class OrderController extends Controller
{

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return $this->str_starts_with($path, "/orders");
    }

    /**
     * @param string[] $split
     */

    public function createNewOrder()
    {
        //TODO
    }

    function viewDestination(array $split)
    {
        //Cant do anything with orders of you're not logged in
        if (!Account::isLoggedIn()) {
            header('location: /');
            exit;
        }
        if (!isset($split[2])) {
            // IF the user requests the main page and not a subpage
            $TITLE = "Flevosap: Bestelgeschiedenis";
            global $orders;
            $db = db();
            $statement = $db->prepare("SELECT *, SUM(i.quantity) totalQuantity,
                                                ROUND(SUM(s.price * i.quantity), 2) totalPrice 
                                                FROM orders o 
                                                JOIN orderitem i ON i.orderID = o.id
                                                JOIN sappen s ON i.sapID = s.id
                                                WHERE userID = ?
                                                GROUP BY o.id");
            $statement->execute([$_SESSION['userID']]);
            $orders = $statement->fetchAll();
            include 'fragments/prefix.php';
            include 'fragments/header.php';
            include 'views/orders/index.php';
            include 'fragments/suffix.php';
        } else {
            // split[2] is het gedeelte achter de 2e slash
            switch ($split[2]) {
                case "test":
                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    echo "test lol";
                    include 'fragments/suffix.php';
                    break;
                default:
                    $orderID = $split[2];
                    global $order;
                    $order = Order::getDetailedOrderInfo($orderID);
                    if ($order[0]['userID'] != $_SESSION['userID']) {
                        header("location:/");
                        die();
                    }
                    include 'fragments/prefix.php';
                    include 'fragments/header.php';
                    include 'views/orders/orderView.php';
                    include 'fragments/suffix.php';
            }
        }
    }
}