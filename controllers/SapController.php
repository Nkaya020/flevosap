<?php
class SapController extends Controller {

    /**
     * @param string $path
     * @return bool
     */
    function doesPathMatch(string $path)
    {
        return ($path=="/sappen");
    }

    /**
     * @param string[] $split
     */
    function viewDestination(array $split)
    {
        $TITLE="Flevosap: Sappen";

        $sappen = Sap::getAll();

        include 'fragments/prefix.php';
        include 'fragments/header.php';
        include 'views/sappen/index.php';
        include 'fragments/suffix.php';
    }
}