<main class="container">
    <h2>Winkelmand</h2>

    <?php if (isset($cartItems) && count($cartItems) > 0) { ?>
        <div class="row valign-wrapper">
            <div class="col s2">
            </div>
            <div class="col s3">
                <h6>Product</h6>
            </div>
            <div class="col s2">
                <h6>Stuksprijs</h6>
            </div>
            <div class="col s2">
                <h6>Stuks</h6>
            </div>
            <div class="col s2">
                <h6>Totaalprijs</h6>
            </div>
            <div class="col s3"></div>
        </div>
        <?php foreach ($cartItems as $item) { ?>
            <hr>
            <form action="/cart/update" method="POST">
                <div class="row valign-wrapper">
                    <div class="col s2">
                        <img src="<?= $item['sap']->image ?>" style="height: 128px" alt="sapje"/>
                    </div>
                    <div class="col s4">
                        <?= $item['sap']->name ?>
                    </div>
                    <div class="col s2">
                        € <?= number_format($item['sap']->price, 2) ?>
                    </div>
                    <div class="col s2">
                        <label>
                            Hoeveelheid
                            <input class="input-field spinner" min="0" name="amount" type="number"
                                   value="<?= $item['quantity'] ?>">
                        </label>
                    </div>
                    <div class="col s2">
                        € <?= number_format($item['sap']->price * $item['quantity'], 2) ?>
                    </div>
                    <div class="col s2">
                        <input type="hidden" name="item" value="<?=$item['sap']->id?>" />
                        <input type="submit" class="input-field btn" value="Update">
                    </div>
                </div>
            </form>
        <?php } ?>
        <hr>
        <br>
        <div class="total-price">
            <table>
                <tr>
                    <td>Subtotaal</td>
                    <td>€ <?= number_format($totalPrice, 2) ?></td>
                </tr>
            </table>
            <div class="row"></div>
            <a href="/cart/checkout" class="btn right green">
                Afrekenen
            </a>
        </div>
        <div class="row"></div>
    <?php } else { ?>
        <div>
            <h5>Je winkelmand is leeg!</h5>
        </div>
    <?php } ?>
</main>