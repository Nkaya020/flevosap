<main class="container">
    <div class="block">
        <h2>Bezorgadres</h2>
    </div>
    <!-- Selecteer Bank met account -->
    <label for="address">Selecteer uw bank:<br></label>
    <select name="bank" id="address">
        <option>ING</option>
        <option>Abn Amro</option>
        <option>Rabobank</option>
        <option>ASN bank</option>
        <option>Bunq</option>
        <option>Knab</option>
    </select>
    <?php if (empty($addresses)) { ?>
        <form action="/cart/checkout" method="POST" class="registerform">
            <!-- Selecteer adres met account -->
            <div class="input-field col s6">
                <select name="address" id="address">
                    <option value="" disabled selected>Kies je bezorgadres</option>
                    <?php foreach ($addresses as $address){ ?>
                        <option value=<?= $address->id ?>><?= $address->name ?></option>
                    <?php } ?>
                </select>
                <label for="address">Bezorgadres:</label>
            </div>
            <!-- Selecteer adres zonder account -->
            <label for="name">Voornaam:</label>
            <input type="text" name="name" required><br><br>

            <label for="lastName">Achternaam:</label>
            <input type="text" name="lastName" required><br><br>

            <label for="city">Woonplaats:</label>
            <input type="text" name="city" required><br><br>

            <label for="address">Adres:</label>
            <input type="text" name="address" required><br><br>

            <label for="phoneNumber">Telefoonnummer:</label>
            <input type="number" name="phoneNumber" required><br><br>
            <br>
            <input class="btn green" type="submit" value="Bezorg op dit adres">
        </form>
    <?php } else { ?>
    <form action="/cart/checkout" method="POST" class="registerform">
        <!-- Selecteer adres met account -->
        <label for="address">Selecteer adress:<br></label>
        <select name="address" id="address">
            <?php foreach ($addresses as $address){ ?>
                <option value=<?= $address->id ?>><?= $address->address ?></option>
            <?php } ?>
        </select>
        <input class="btn green" type="submit" value="Bezorg op dit adres">
        <a class="btn" href="/accinfo/new" class="registerform">Of voeg hier een nieuw adres toe!</a>
    </form>
    <?php } ?>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('select');
            var instances = M.FormSelect.init(elems, null);
        });
    </script>
</main>