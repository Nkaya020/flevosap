<main class="container">
    <div class="row"></div>
    <div class="row red z-depth-2 center-text">
        <?php if (array_key_exists('fail', $_GET)) {
            echo "<h5 class='center-align col s12'>Verkeerde logingegevens. Probeer het nog een keer?</h5>";
        } ?>
    </div>
    <div class="row valign-wrapper">
        <div class="col s12 m5 z-depth-2">
            <div class="row"></div>
            <h4>Wat leuk dat je er weer bent!</h4>
            <form action="/account/login" method="post" class="inloggen">
                <div class="input-field col s6 inline">
                    <label for="user">Email</label>
                    <input id="user" placeholder="mijn@email.nl" type="text" name="email" required>
                </div>
                <div class="input-field col s6 inline">
                    <label for="user_pass">Wachtwoord</label>
                    <input id="user_pass" type="password" placeholder="•••••••••••••" name="password" required>
                </div>
                <button class="btn right" type="submit">Login</button>
                <div class="row"></div>
            </form>
        </div>
        <div class="col s12 m2">
            <h3 class="center-align">of...</h3>
        </div>
        <div class="col s12 m5 z-depth-2">
            <h4>Wat leuk dat je bij ons komt winkelen!</h4>
            <a class="btn right" href="/account/new">Registreer!</a>
            <div class="row"></div>
        </div>
    </div>
</main>