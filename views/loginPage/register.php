<main class="container">
    <div class="row">
        <h2>Vul alstublieft uw gegevens in.</h2>
    </div>
    <div class="block_row_1">
        <form class="registerform" method="POST"  action="/account/new">
            <div class="registerinput required">
                <label for="voornaam">Email:</label>
                <input name="email" type="email" id="mail" placeholder="Email" required>
            </div>

            <div class="registerinput">
                <label for="tussenvoegsel">Wachtwoord:</label>
                <input name="password" type="password" id="pw" placeholder="Wachtwoord">
            </div>
            <div class="registerinput">
                <label for="tussenvoegsel">Bevestig wachtwoord:</label>
                <input name="password2" type="password" id="pw2" placeholder="Wachtwoord">
            </div>
            <input class="btn" type="submit" id="submit" value="Wordt lid!">
        </form>
    </div>
</main>