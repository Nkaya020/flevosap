<main class="container">
    <div class="row">
        <h2>Vul alstublieft uw gegevens in.</h2>
    </div>
    <div class="block_row_1">
        <form class="registerform" action="/accinfo/new" method="POST">
            <div class="registerinput required">
                <label for="voornaam">Voornaam:</label>
                <input type="text" id="voornaam" placeholder="Voornaam" pattern="[^0-9]+" required>
            </div>

            <div class="registerinput">
                <label for="tussenvoegsel">Tussenvoegsel:</label>
                <input type="text" id="tussenvoegsel" placeholder="Tussenvoegsel" pattern="[^0-9]+">
            </div>

            <div class="registerinput required">
                <label for="achternaam">Achternaam:</label>
                <input type="text" id="achternaam" placeholder="Achternaam" pattern="[^0-9]+" required>
            </div>

            <div class="registerinput required">
                <label for="geboortedatum">Geboortedatum:</label>
                <input type="date" id="geboortedatum" value="<?php echo date('Y-m-d'); ?>" required>
            </div>

            <div class="registerinput required">
                <label for="postcode">Postcode:</label>
                <input type="text" id="postcode" placeholder="1234 AB" pattern="[0-9]{4} [A-Z]{2}" required>
            </div>

            <div class="registerinput required">
                <label for="woonplaats">Woonplaats:</label>
                <input type="text" id="woonplaats" placeholder="Woonplaats" pattern="[^0-9]+" required>
            </div>

            <div class="registerinput required">
                <label for="adres">Adres:</label>
                <input type="text" id="adres" placeholder="Adres + huisnummer" required>
            </div>

            <div class="registerinput required">
                <label for="telefoonnummer">Telefoonnummer:</label>
                <input type="tel" id="telefoonnummer" placeholder="000-000000" pattern="0\d{1,3}[-|\s]?\d{6,8}" minlength="10" maxlength="11" required>
            </div>
            <input class="btn" type="submit" id="submit" value="Bevestig adres">
        </form>
    </div>
</main>