<main class="container">
    <div class="block">
        <p>
        <h2>Adresgegevens</h2>
        </p>
    </div>
    <div>
        <table id="customers" class="table">
            <thead>
            <tr>
                <th>Naam</th>
                <th>Stad</th>
                <th>Adres</th>
                <th>Telefoonnummer</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($addresses as $address) { ?>
                <tr>
                    <td>
                        <?= $address->name ?>
                    </td>
                    <td>
                        <?= $address->city ?>
                    </td>
                    <td>
                        <?= $address->address ?>
                    </td>
                    <td>
                        <?= $address->phonenumber ?>
                    </td>
                    <td>
                        <form class="input-field" action="/accinfo/delete" method="post">
                            <input type="hidden" name="id" value=<?= $address->id ?>>
                            <input class="btn red" type="submit" value="Adres verwijderen">
                        </form>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="row"></div>
        <a class="btn" href="/accinfo/new">Adres toevoegen</a>
    </div>
    <div class="row"></div>
</main>