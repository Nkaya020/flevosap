<main class="container">
    <h1>Bestelgeschiedenis</h1>

    <br>

    <?php
    global $orders;
    if(count($orders)< 1){?>
        <h4>U heeft nog geen bestelling. Klik <a href="/sappen">hier</a> voor het maken van uw eerste bestelling.</h4>
        <div class="row"></div>
    <?php } else { ?>

    <br>

    <div class="row">
        <div class="col s3">
            Datum
        </div>
        <div class="col s3">
            Producten
        </div>
        <div class="col s3">
            Totaalprijs
        </div>
    </div>
    <?php
    }
    foreach($orders as $order) { ?>
        <hr>
        <div class="row">
            <div class="col s3">
                <?=$order["createdAt"]?>
            </div>
            <div class="col s3">
                <?= $order["totalQuantity"] ?>
            </div>
            <div class="col s3">
                € <?= $order["totalPrice"] ?>
            </div>
            <div class="col s3">
                <a class="btn" href="/orders/<?=$order["orderID"]?>">Bekijk</a>
            </div>
        </div>
    <?php } ?>
</main>