<main class="container" xmlns="http://www.w3.org/1999/html">
    <?php global $order, $orderID; ?>
    <div class="block"></div>
    <div class="block">
        <h2>Bestelling <?=$orderID?></h2>
    </div>
    <div class="row">
        <p>Datum: <?=$order[0]['createdAt']?></p>
    </div>
    <div class="row">
        <div class="col s2">
            Afbeelding
        </div>
        <div class="col offset-s1 s2">
            Naam
        </div>
        <div class="col offset-s1 s1">
            Stukprijs
        </div>
        <div class="col offset-s1 s1">
            Aantal
        </div>
        <div class="col offset-s1 s2">
            Totaalprijs
        </div>
    </div>
    <?php foreach($order as $item) { ?>
        <hr>
        <div class="row">
            <div class="col s2">
                <img src="<?=$item['image']?>" style="height: 128px">
            </div>
            <div class="col offset-s1 s2">
                <?=$item['name']?>
            </div>
            <div class="col offset-s1 s1">
                <?=$item['price']?>
            </div>
            <div class="col offset-s1 s1">
                <?=$item['quantity']?>
            </div>
            <div class="col offset-s1 s2">
                € <?=$item['quantity'] * $item['price']?>
            </div>
        </div>
    <?php } ?>
</main>