<main class="content container">
    <div class="block">
        <!-- image and advertisement goes here -->
    </div>
    <div class="block"></div>
    <div class="block">
        <h2>Onze sappen</h2>
        <p>
            Dit zijn ze dus! De meeste (s)maken we het hele jaar door, sommige volgens een
            speciale receptuur alleen in een bepaald seizoen. Lees daarom dus ook regelmatig onze nieuwtjes.
            Overigens is Flevosap BRC-gecertificeerd.
            Dit betekent dat de British Retail Consortium ons ieder jaar test op hygiëne- en voedselveiligheid.
        </p>
    </div>
    <div class="catalog">
        <?php
        foreach ($sappen as $sap) {
            ?>
            <div class="row">
                <div class="col s12">
                    <div class="card grey lighten-5 horizontal">
                        <div class="card-image">
                            <img class="card-image"
                                 src=<?= $sap->image ?> alt="afbeelding"/>
                        </div>
                        <div class="card-content">
                            <h3 class="card-name"><?= $sap->name ?></h3>
                            <?= $sap->description ?>
                        </div>
                        <form action="/cart/add" class="card-action" method="POST">
                            <h5 class="col m12 s12">€<?= number_format($sap->price, 2) ?><br><small>per stuk</small></i></h5>
                            <input name="productID" type="hidden" value="<?= $sap->id ?>">
                            <input name="quantity"  class="input-field col m12 s12" type="number" min="1" value="1">
                            <!-- input "productID" bevat het ID van de sap die je wil toevoegen aan de winkelwagen. -->
                            <button class="btn input-field"  style="width: 100%;" type="submit"><i class="material-icons">add_shopping_cart</i></button>
                            <?php if(isset($_SESSION['cart'][$sap->id])) { ?>
                            <a href="/cart" class="btn green" style="width: 100%;"><?= $_SESSION['cart'][$sap->id] ?></a>
                            <?php } ?>
                        </form>
                        <div class="card-reveal">
                            <h3>Voedingswaarde per 100 gram<br/></h3>
                            <?= $sap->nutrition ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</main>