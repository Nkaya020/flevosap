<main class="container">
    <div class="row">
        <p>
            Liefhebbers van Flevosap: welkom!

            De vele variaties zorgen voor de ene fruitige verrassing na de andere.
            De pure smaak van één soort, of een spannende combinatie van twee of meer.
            Altijd 100 procent natuurlijk, dus zonder smaak- en conserveringsmiddelen.
            Waar je ook bent, met Flevosap heb je altijd de smaak te pakken!
        </p>
    </div>
    <div class="block">

    </div>
    <div class="block">
        <h3>Altijd en overal genieten</h3>
        <p>
            De grote flessen kom je tegen in je supermarkt. Dat is makkelijk meenemen, en kinderen zijn er dol op.
            Volwassenen trouwens ook. In de winkel vind je ook een handig klein flesje dat graag mee onderweg gaat.
            Zoals naar (sport)school of werk, een dagje uit, of in je rugzak op vakantie.
            Gezellig samen wat eten en drinken in een café, lunchroom of restaurant?
            Ook daar kun je genieten van de smaak van vers fruit. Om zeker te weten dat je de echte krijgt,
            bestel je niet zo maar een sapje, maar vraag je om Flevosap!
        </p>
    </div>
</main>