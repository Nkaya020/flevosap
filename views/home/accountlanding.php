<main class="container">
    <div class="row">
        <h1>Welkom!</h1>
        <a class="btn" href="/orders"><span>Ordergegevens</span></a>
        <a class="btn" href="/accinfo"><span>Adresgegevens</span></a>
    </div>
    <div class="block">
        <p>
            <a class="btn" href="/account/logout"><span class="headerButton">Uitloggen</span></a>
        </p>
    </div>
</main>