<?php
require '.db/require.php';

$db = db();

$db->exec(file_get_contents(".db/clear.sql"));

$db->beginTransaction();

// CREATE OR REPLACE TABLEs
foreach(glob(".db/structure/*.sql") as $script) {
    runScriptOrDie($script, $db);
}

// Create relations on tables
foreach(glob(".db/relations/*.sql") as $script) {
    runScriptOrDie($script, $db);
}

// Fill tables with data
foreach(glob(".db/data/*.sql") as $script) {
    runScriptOrDie($script, $db);
}

function runScriptOrDie(string $script, PDO $database) {
    $return = $database->exec(file_get_contents($script));
    if ($return === false) {
        echo $script . " failed to run:<br>";
        die(print_r($database->errorInfo()));
    } else {
        echo $script . " ran successfully.<br>";
    }
}

$stmt = $db->prepare("SELECT id, password FROM account");
$stmt->execute(null);
foreach($stmt->fetchAll() as $user) {
    $stmt = $db->prepare("UPDATE account SET password = ? WHERE id = ?");
    $stmt->execute([password_hash($user['password'], PASSWORD_DEFAULT), $user['id']]);
}
echo "Hashed passwords.<br>";

$db->commit();

?>